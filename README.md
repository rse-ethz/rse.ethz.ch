# About

This is the source for the website at [https://rse.ethz.ch](https://rse.ethz.ch).

The website is built using `Hugo` and a slightly modified version of the Hextra theme.

## Setup

Please install `devbox`, for installation instructions see https://www.jetify.com/devbox/docs/installing_devbox/.

To contribute content to the website, please create a fork. You can find instructions on how to
create a fork at https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#create-a-fork


### Manual Installation

If you don't want to use devbox, please don't forget to install the submodules:

```
$ git submodule init
$ git submodule update
```

## Working on the content of the website


### Starting the web server

You can build a local version and start a webserver now using

```
$ devbox run server
```

This will serve the website at [http://localhost:1313](http://localhost:1313) or
on a different port.

Please **read the exact output**, to see the exact URL.

### Updating the content

Make sure that the you started the web server first.

Then open another terminal window or tab  and activate the development
environment using

```
$ devbox shell --pure
```

You will see a different prompt in your shell after executing this command.

The content of the website is in the `src/content` folder. For a more detailed
overview of the folder structure [read here.](https://imfing.github.io/hextra/docs/guide/organize-files/)

You can find examples to create events in `src/content/events` and examples to create a blog post
in `src/content/blog`.

Please use underscores and lower case letters for the file names. No spaces, upper case or special characters.
This is specifically important when developing under MacOS, because it's file system will serve mixed-case
filenames, but your Linux server probably will not.

`Hugo` will detect changes and then build and serve the updated web site:

- in most cases you will see the changes in the browser without reload.
- in case the website does not update, `Hugo` did not recover from an error and you
  have to stop the server (in the other terminal window resp. tab) and run `devbox run hugo` again.


### Create merge request

1. create a  new branch in git with a descriptive name.

2. create a commit with your changes, the spellchecker will check the content and you may need to add new
   lines to `spellchecker-ignore.txt` in case the spellchecker detects unknown words.

3. push the content to your fork

4. Navigate to your fork on `gitlab.com` and you should see a text field with the option to create a merge request.

5. If the merge request is accepted, the CI server will update the website at `rse.ethz.ch`


### Helper scripts

- `devbox run create-event`  is a command-line tool to create a
  minimal event announcement. Please use the `--help` flag for further
  information.
