# create thumbnails using imagemagick
# https://imagemagick.org/script/download.php

for IN in $(find . -name '*.png' -or -name '*.jpeg' ); do

    if [[ $IN =~ thumbnail\.(png|jpeg) ]]; then
        continue
    fi

    OUT=${IN%.*}-thumbnail.png;
    if test -f $OUT; then
        echo "skip $IN"
        continue;
    else
        convert $IN -thumbnail '200x200>' $OUT
        echo "wrote $OUT"
    fi
done
