+++
title = 'About'
toc = true
+++

## Who are we?

The RSE community at ETH is currently organised by
the RSE working group. You can find out more about us
[here](/working_group).

## Are you an RSE?

There is no unique definition for "what is an RSE" and people
self-identify as an RSE. You may consider yourself being an RSE if you
can answer **at least one** of the following questions with a "yes":

- I like programming as much as science, and maybe a little bit more.
- I use programming expertise to advance research.
- I apply software engineering skills and practices to research to
  create more robust, manageable, and sustainable research software.

<br/>
<figure>
  <blockquote>
  The term Research Software Engineer, or RSE, emerged a little over 10
  widely adopted and there are a number of high-level definitions of
  what an RSE is. However, the roles of RSEs vary depending on the
  institutional context they work in. At one end of the spectrum, RSE
  roles may look similar to a traditional research role. At the other
  extreme, they resemble that of a software engineer in industry. Most
  RSE roles inhabit the space between these two extremes. Therefore,
  providing a straightforward, comprehensive definition of what an RSE
  does and what experience, skills and competencies are required to
  become one is challenging.
  </blockquote>
  <figcaption>
  <cite>
    Source: <a href="https://doi.org/10.12688/f1000research.157778.1">
    https://doi.org/10.12688/f1000research.157778.1"</a>
  </cite>
  </figcaption>
</figure>
<br/>


<center>
  <figure>
      <img src="rse-spectrum.svg" width="70%" alt="spectrum of rses"
           style="padding: 2.5em 0.8em 0.3em 0.8em; background: #f5f5f5"/>
      <figcaption>The "RSE Spectrum"</figcaption>
  </figure>
</center>

The title "RSE" was invented with several objectives in mind:

  - To give people with overlapping profiles and interests a common
    identity and increase their visibility.

  - To provide a unique job title that is easier to search for and to
    advertise than all the little variations like "scientific
    programmer", "research software specialist", etc.

  - To highlight the importance of software (especially sustainable
    software development) and those who develop it in modern
    research.

  - To create communities of like-minded people who share common
    interests.


## The US definition

We embrace the
[definition](https://us-rse.org/about/what-is-an-rse)
given by the {{< abbrev USRSE >}} is:

>    We like an inclusive definition of Research Software Engineers to
>    encompass those who regularly use expertise in programming to
>    advance research. This includes researchers who spend a significant
>    amount of time programming, full-time software engineers writing
>    code to solve research problems, and those somewhere in-between. We
>    aspire to apply the skills and practices of software development to
>    research to create more robust, manageable, and sustainable
>    research software.


## It started in the UK

Our initiative builds on 10 years of work by others.

The {{< abbrev "SSI" >}} conducted a study in 2014 to access the
importance of Software in UK Research.
The conclusion was that around 70% of researchers said they could not
carry out their research without *research software*[^1]. Another
finding was that most researchers who develop research software have no
formal training in software development.
[This blog post](https://www.software.ac.uk/blog/its-impossible-conduct-research-without-software-say-7-out-10-uk-researchers)
discusses the results of this study in more detail.

The first action taken to highlight the special role of the programmers who
develop research software was to create a new and unique name: **Research
Software Engineers**.

We recommend [this article](https://www.software.ac.uk/blog/not-so-brief-history-research-software-engineers-0)
for a full overview.


[^1]: <q>Software that does not generate, process or analyse results - such as word
processing software, or the use of a web search - does not count as 'research
software' for the purposes of this survey.</q>
