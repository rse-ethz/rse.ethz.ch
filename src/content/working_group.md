+++
title = 'RSE Working Group'
toc = true
+++

THE RSE Working Group is a group of volunteers who organise an RSE community at
ETH Zurich.

We meet every two weeks on Zoom. If you are interested in helping us or want to
contribute to the community in any way,
please email <span class="email">uwe.sc<b>asdf</b>hmit<b>d</b>t@id.<b>.</b>ethz.ch</span>.

## Who are we?


### Franziska

My name is Franziska Menti and I work part time as science support in the
exoplanets and habitability group  at ETH Zurich. My main project is to design
and implement  the LIFE (Large Interferometer For Exoplanets) target database.
In my free time, I like interacting with animals, playing (board) games and
helping our society become more inclusive.

<details>

<summary>More about Franziska</summary>

**My background**
I studied physics at ETH Zurich. After finishing my master there I am now
employed through *Workaut* which offers a program for inclusion of autistic
people in workplaces. While I disliked programming during my studies since I
have been able to apply it to astrophysics I started enjoying it more and more.

**RSE**
I first heard the term RSE at the kick off event of the RSE community. There I
realised that all those years I have actually been a research software engineer
without knowing it. My favourite part about RSE is the combination of
fascinating research topic with nice structure through code.

</details>


### Jaime

Hi! My name's Jaime Cardozo and I work as software engineer at ETH Library Zurich.
I started my professional career in IT with an apprenticeship as application
developer almost fifteen years ago and been around since then, always staying
curious!

<details>

<summary>More about Jaime</summary>

**My motivation**

I am part of this community to deepen my knowledge and also connect to other people
working in research, supporting each other with the great challenges we face!

**Now**
My team and I care about digital long-term preservation and are responsible for
the data archive at ETH. I design middleware applications and data pipelines to
ensure smooth travel of various assets entering the digital chronicles.

</details>



### Roman

My name is Roman Wixinger and I work as a Data Scientist at Ergon Informatik. I
am an active member of the RSE@ETHZ community because I believe that software
engineering is the missing piece to effective (data) science.

<details>

<summary>More about Roman</summary>

**My background**
My journey started as a physics student at ETH Zurich, where I discovered my
passion for solving problems with software. During my studies, I had the chance
to write software at various research institutes and companies.

**My motivation**
During a particular experience at the University of Tokyo, I realised that
software engineering craftsmanship is crucial for effective and reproducible
research. With a few changes in the experimental workflow, we could automate
the data taking and analysis, saving countless hours and enhancing
reproducibility. This is why I believe that it is our duty as RSE community to
spread awareness about software engineering best practices and invest in
teaching.

</details>


### Uwe

My name is Uwe Schmitt and I work as a Senior RSE at the Scientific IT Services
of ETH Zurich (https://sis.id.ethz.ch). In this position I develop
software for research and help researchers in many ways with IT-related
tasks. I also enjoy teaching courses from time to time which
helps me to stay in touch with researchers and which I also like as a
change from the work routine.

<details>

<summary>More about Uwe</summary>

**My background**
I studied mathematics in Saarbrücken, Germany, and have a PhD in applied
mathematics. I have always enjoyed the programming part of my research, because
it can turn theoretical considerations into something of practical use. I
started to learn about software engineering practices after my PhD because I
wondered how programmers could write and maintain robust programs with 10,000
lines of code and more.

**My favourite parts of being an RSE**
I really enjoy working on many different projects in many different scientific
areas. Seeing that my work has a direct impact on the researchers I'm working
with is also very rewarding and motivating. I also enjoy the flat hierarchy and
flexibility of my working environment.

**My least favourite parts of being an RSE**
Working on many projects at the same time can be tiring and stressful. As many
of my projects are not very big, I often work as a one-person team.

</details>
