+++
date = 2024-02-18T13:52:55+01:00
toc = true
+++

# Research Software Engineering Community

Software is ubiquitous in modern research and its quality has a direct
impact on the quality of research. The Research Software Engineering
Community connects Research Software Engineers (RSEs), increases their
visibility and works towards the recognition of their importance in
science.

Although this website (still) has an `ethz.ch` domain, the community is
not intended to be ETHZ only and we invite everyone who identifies as an
RSE to join us and participate in our events.

{{< callout type="info" emoji="🤔" >}}
[Do you wonder if you are an RSE?](/about#are-you-an-rse)
{{< /callout >}}


## Latest blog entries

{{< latest_blog_entries >}}

## Upcoming events

{{< upcoming_events >}}

We announce upcoming events and list past events at [Events](/events).

## How to stay in touch?

Our mailing list provides updates about our [events](/events) and
activities:

<center class="my-2">
  <form action="https://sympa.ethz.ch/sympa/subscribe/rse-community">
      <input type="submit" class="my-2 btn btn-blue text-white font-bold py-2 px-4 rounded" value="Subscribe now!"/>
  </form>
</center>
<p>

You will receive an email after you have registered to **confirm that
you are registering for yourself**.

This is an overview of our
[communication channels](/communication):

{{< cards cols="3" >}}
  {{< card link="/communication#matrix"
     title="We are chatting on Matrix!"
     image=""
     subtitle="Learn how to join our Matrix space."
  >}}
  {{< card link="/communication#rss-feed"
     title="The blog and events pages support RSS"
     image=""
     subtitle="Use an RSS reader or subscribe with <code>feedrabbit.com</code> to get email updates."
  >}}
  {{< card link="/communication#mailinglist"
     title="Subscribe to our Mailing List!"
     image=""
     subtitle="Read more about the mailing list."
  >}}
{{< /cards >}}




## Imprint


We are researchers, data scientists and software engineers working at {{< abbrev "ETHZ" >}}. This
website is an attempt to communicate our current activities and to increase
visibility of the community.  You can find out more about us [here](/working_group).

Please email <span class="email">uwe.sc<b>asdf</b>hmit<b>d</b>t@id.<b>.</b>ethz.ch</span>
in case you want to contact us.
