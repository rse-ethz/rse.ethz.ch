---
title: Curated list of RSE related courses and lectures
---


|Course|Lecturer|Organisation|Topics (RSE related)|Target Audience|Link|
|:----|:----|:----|:----|:----|:----|
|Basics of Computing Environments for Scientists |Christian Herzog|ETH Zurich, Department Physik|Linux, Python, Packaging, Reproducibility|Bachelor, Masters and PhD Students|[link](https://www.vorlesungen.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?semkez=2024S&ansicht=KATALOGDATEN&lerneinheitId=176608&lang=de)|
|Best Practices in Programming|Various, depends on availability|Swiss Institute of Bioinformatics|Programming, Unit testing, Refactoring, Version control|Generally open, with fee|[link](https://www.sib.swiss/training/course/20230705_BPP)|
|Datenanalyse in der Physik|Alexander Eichler, Martin Kroner|ETH Zurich, Department Physik|Experimentation, Python, Statistics|Physics Bachelor Students|[link](https://www.vorlesungen.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?semkez=2024S&lerneinheitId=178564&ansicht=ALLE)|
|Hacking for Science|Matthias Bannert|ETH Zurich, KOF Swiss Economic Institute|A Guide to the Open Source Ecosystem|PhD candidates (but open to guests)|[link](https://rseed.ch/startpage/teaching/)|
|Introduction to Machine Learning using Python|Uwe Schmitt, Tarun Chadha, Franziska Oschmann|ETH Zurich, Scientific IT Services|Machine Learning|Researchers|[link](https://sissource.ethz.ch/sispub/courses/machinelearning-introduction-workshop)|
|Introduction to the Python Programming Language|Uwe Schmitt|ETH Zurich, Scientific IT Services|Learning Python for attendees with previous programming experience|Members of Swiss universities|[link](https://siscourses.ethz.ch/python/the_python_programming_language.html)|
|Numerical Methods |Vasile Gradinaru|ETH Zurich, Department Mathematik|Numerical computations with Python|BSc Physics Students|[link](https://www.vorlesungen.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?semkez=2024S&ansicht=KATALOGDATEN&lerneinheitId=177912&lang=de)|
|Open Science Tools - Authoring and Publishing Workflows for Collaborative Scientific Writing|Lars Schöbitz|ETH Zurich, D-MAVT|8-hour workshop: Quarto documents/slides/websites, git version control (via GUI), GitHub collaboration (issue tracker), GitHub Pages publishing|PhD|[link](https://ost-hs23.github.io/website/)|
|Programming Techniques for Scientific Simulations|Roger Käppeli|ETH Zurich, Department Mathematik|Simulation, Scientific Software Libraries|Scientific Computing Bachelor Students|[link](https://www.vvz.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?semkez=2023W&ansicht=KATALOGDATEN&lerneinheitId=172964&lang=de)|
|Quarto - authoring and publishing tools for collaborative scientific writing|Lars Schöbitz|ETH Zurich, D-MAVT|2-hour workshop: Quarto documents/slides/websites, quartopub.com as a service to publish static websites|PhD, Research Data Management summer school|[link](https://quarto-rdmss-24.github.io/website/)|
|Quarto - authoring and publishing tools for collaborative scientific writing|Lars Schöbitz|ETH Zurich, D-MAVT|4-hour workshop: Quarto documents/slides/websites, quartopub.com as a service to publish static websites|PhD|[link](https://quarto-ambition.github.io/website/)|
|Research Beyond the Lab: Open Science and Research Methods for a Global Engineer|Lars Schöbitz, Elizabeth TIlley|ETH Zurich, D-MAVT|R programming, Quarto documents, git version control (via GUI), GitHub collaboration (issue tracker),  GitHub Pages publishing, open data, data management|BSc, MSc, PhD|[link](https://rbtl-fs24.github.io/website/)|
|Rigorous Software Engineering |Martin Vechev, Malte Schwerhoff|ETH Zurich, Department Informatik|Code documentation, Modularity and coupling, Program analysis, Formal modeling|CS Bachelor Students|[link](https://www.vvz.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?semkez=2024S&ansicht=STUDPLANINFO&lerneinheitId=177126&lang=en)|
|Scientific programming with Python|Federica Lionetto|UZH, Faculty of Science, Physik Institut|Best practices, Data structures, Cython, C++| |[link](https://www.physik.uzh.ch/~python/python/index.php)|
|The missing semester of your CS education||MIT|Shell, version control, ...||[Link](https://missing.csail.mit.edu/)|
