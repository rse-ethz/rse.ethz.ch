+++
date = 2024-02-18T13:52:55+01:00
toc = true
+++

# Communication channels


## Matrix / Element


### About

[Matrix](https://matrix.org) is an instant messaging protocol used to
provide chat rooms. Compared to other solutions, such as
[Slack](https://slack.com), Matrix provides end-to-end encryption and
runs on distributed servers. Some universities or university departments
in Switzerland and Germany run their own Matrix servers. More details
about Matrix can be found
[here](https://joinmatrix.org/guide/#why-matrix).
To see how Matrix compares to some of the alternatives, click
[here](https://joinmatrix.org/guide/matrix-vs-al/).

To communicate using the Matrix protocol, you need a so called
[Matrix client](https://matrix.org/ecosystem/clients/).
[Element](https://element.io/) is a popular one, which can used in a
web browser, a desktop app or on a mobile phone.


### How to join

The ID of the RSE matrix space (the umbrella for our chat rooms) is
`#rse-ch:matrix.org`.

This link will offer you several options to connect:
[https://m.ethz.ch/#/#rse-ch:matrix.org](https://m.ethz.ch/#/#rse-ch:matrix.org)

  - In case you work for ETH Zurich you can find some help
    [at the ETH wiki](https://unlimited.ethz.ch/display/itkb/Get+started).
  - If you are a student, you can use the
    [same instructions](https://unlimited.ethz.ch/display/itkb/Get+started)
    but replace `staffchat` by `student`.
  - In all other cases check out if your institution offers matrix accounts, or
    [read the matrix documentation](https://matrix.org/docs/older/introduction/#how-can-i-try-it-out) how to create one.
  - Please check [this post](https://frontpagelinux.com/tutorials/beginners-guide-how-to-get-started-with-element-matrix/)
    on "how to get started with Element & Matrix" also.


## RSS Feed


An RSS feed is a way for a website to announce updates to so-called RSS
readers (also called aggregators). The advantage of this method is that
the RSS reader collects news from  many sources, such as newspapers or
blogs. If you use an RSS reader resp. aggregator, you can follow
sources from one place.

Here are some options to follow an RSS feed:

  - [feedly.com](https://feedly.com) is a popular RSS aggregator which
    works in the browser.
  - The [Thunderbird](https://en.wikipedia.org/wiki/Mozilla_Thunderbird)
    email client supports RSS feeds. [This blog post explains the
    details](https://blog.thunderbird.net/2022/05/thunderbird-rss-feeds-guide-favorite-content-to-the-inbox/).
  - [feedrabbit.com](https://feedrabbit.com/) delivers updates to RSS
    feeds per email. The service is free for up to 10 feeds.


[Wikipedia](https://en.wikipedia.org) offers a [comprehensive list of RSS readers and aggregators](https://en.wikipedia.org/wiki/Comparison_of_feed_aggregators).


{{< callout type="info" >}}

To subscribe to updates from the **blog** from an RSS reader, please
add the URL
[https://rse.ethz.ch/blog/index.xml](https://rse.ethz.ch/blog/index.xml)
to your aggregator.
For **event updates** use
[https://rse.ethz.ch/blog/index.xml](https://rse.ethz.ch/events/index.xml).
{{< /callout >}}


## Mailing List

Our mailing list provides updates about our [events](/events) and
activities. Currently we are sending up to 3 emails per month.

The mailing list is provided by
[https://sympa.ethz.ch/](https://sympa.ethz.ch/). The URL to manage a subscription is
[https://sympa.ethz.ch/sympa/subscribe/rse-community](https://sympa.ethz.ch/sympa/subscribe/rse-community).

After registering, you will receive an email to confirm that you are
registering for yourself.
