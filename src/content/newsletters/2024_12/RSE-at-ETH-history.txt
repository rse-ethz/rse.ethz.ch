graph TD
    A[December 2023: Initial Outreach] --> B[February 2024: Official Launch of RSE@ETH]
    B --> C[March 2024: Community Website Goes Live]
    C --> D[June 2024: Launch of the RSE Book Club]
    D --> E[September 2024: Participation in RSECon24]
    E --> F[October 2024: Panel Discussion on Applying for RSE Jobs]
    F --> G[November 2024: Funding Request Approved]

    style A fill:#215caf,stroke:#000000,stroke-width:2px,color:#FFFFFF
    style B fill:#215caf,stroke:#000000,stroke-width:2px,color:#FFFFFF
    style C fill:#215caf,stroke:#000000,stroke-width:2px,color:#FFFFFF
    style D fill:#215caf,stroke:#000000,stroke-width:2px,color:#FFFFFF
    style E fill:#215caf,stroke:#000000,stroke-width:2px,color:#FFFFFF
    style F fill:#215caf,stroke:#000000,stroke-width:2px,color:#FFFFFF
    style G fill:#215caf,stroke:#000000,stroke-width:2px,color:#FFFFFF
