---
title: 'Newsletter December 2024'
sidebar:
  exclude: true
---

Dear all,

It's been a year since we organised the first Zoom call on 12 December
2024 with
[Prof Simon Hettrick](https://www.software.ac.uk/our-people/simon-hettrick)
presenting the history and current statue of the Research Software
Engineering community in the UK.

This newsletter is intended to look back at the previous year and to
share our ideas and plans for 2025.


<!--more-->

### What happened in 2024?

  - We have organised [8 events](https://rse.ethz.ch/events/) in 2024, some
    on Zoom, a few more in person, all dedicated to different topics hoping
    to cover the breadth of RSE related topics and interests.

  - We have meanwhile 320 subscribers on our mailing list, and
    regular posts on our Matrix Chat. The website https://rse.ethz.ch
    counted almost 4000 unique visitor between April and November.

  - EPFL started community activities in May 2024, you can find more
    details and a list of past events at [rse.epfl.ch](https://rse.epfl.ch/).

  - We got some funding from [enhanceR.ch](https://www.enhancer.ch/) and
    from  the
    [ETH-ORD programme](https://open-research-data-portal.ch/services-projects/rse4ord-building-a-research-software-engineering-community-to-promote-open-science/)
    to continue building RSE communities in all ETH domain institutions
    (EPFL, EAWAG, EMPA, PSI, WSL and ETHZ) and to promote the FAIR
    principles for research software.

  - One of our personal highlights was the
    ["RSE Jobs" panel discussion](/events/2024_10_15_rse_jobs_panel/)
    which also resulted in a
    [blog post](https://rse.ethz.ch/blog/2024_10_26_rse_job_panel/)
    written by community members to share their notes for everyone who
    could no participate in the event.

  - We presented our activities at the [German RSE
    Conference](https://events.hifis.net/event/994/) in March 2024
    and the [UK conference](https://rsecon24.society-rse.org/) in
    September 2024.

### Looking forward to 2025

This is what we are discussing and planning for the next year right now:

- A git workshop for beginners and intermediate users in February.

- Zoom presentations in January and later during the year.

- A hackathon in collaboration with
  [ord-hackthon.ch](https://www.ord-hackathon.ch)
  during the year. This is still in the early planning phase and we hope
  that we can organize this before summer.

- We are organizing RSE information events at the RSE domain and other
  Swiss academic institutions within the
  [RSE4ORD](https://open-research-data-portal.ch/services-projects/rse4ord-building-a-research-software-engineering-community-to-promote-open-science/).
  project. The first such event will take place 3 February at
  [WSL in Birmensdorf](https://www.wsl.ch/de/ueber-die-wsl/standorte/wsl-birmensdorf/).
  For details
  [see here](https://rse.ethz.ch/events/2025_02_03_wsl_community_meeting/)


### Impressions

*Personal Impressions from Jaime Cardozo (ETH Library)*

The collaboration with Linus Gasser and Roman Wixinger began naturally
within the RSE community, when I found out they were preparing a
presentation on isolated development shells. I recognized a similar use
case in my own team, where managing various programming languages and
frameworks made switching between code bases a daunting task that I
wanted to simplify.

By reading various materials and discussing these ideas with them, I was
able to gain a much deeper understanding of the subject. In return, I
was able to provide them with some feedback in advance of
their talk at the RSE conference in Newcastle.

Introducing new tooling can increase the mental load on team members, so
I’m cautious and take the time to thoroughly understand the underlying
issues before adopting any solutions. Inspired by their findings, I
created a simple demo project as a proof of concept, which is now
[available publicly](https://github.com/eth-library/devbox-spring-demo)
for others to explore and learn from.

Isolated environments can help eliminate dependency conflicts and
unpredictable setups, ultimately enhancing both productivity and
maintainability. As a result, I’m now happily starting to roll out Devbox
gradually to our repositories, ensuring my team can enjoy a smoother
development experience.

So far, it’s been a great start, and I’m eager to see how future
collaborations within the research software engineering community will
bring us forward!

### A few pictures

<center>
  <figure>
    <img src="conferce_picture.png" width='50%'/>
    <figcaption>RSE Conference UK 2024</figcaption>
  </figure>
</center>

<center>
  <figure>
    <img src="picture-01.jpeg" width='50%'/>
    <figcaption>First Meetup February 2024</figcaption>
  </figure>
</center>

### Get involved

Join us and become a member of the RSE community!

- [Mailing list](https://rse.ethz.ch/#how-to-stay-in-touch): Subscribe [here](https://sympa.ethz.ch/sympa/subscribe/rse-community?)
- [Matrix chat](https://rse.ethz.ch/communication/#matrix--element): Connect with peers on Matrix
- [Working group](https://rse.ethz.ch/working_group/): Participate in our working group
- [Website & blog](https://rse.ethz.ch/blog/): Visit our website and stay updated with our blog

Feel free to forward this newsletter to your colleagues or other
interested parties.

### Acknowledgments

We want to thank our partners and supporters, including ETH Media and
all the volunteers who contributed their time and expertise. Our
activities during the last year would not have been possible without the
continued help of RSEs from many other countries.

We hope you enjoy the holiday season and are looking forward to seeing
you in the new year!

Best wishes,

Franziska, Jaime, Roman, Tarun and Uwe
