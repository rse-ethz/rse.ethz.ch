---
title: Renku 2.0 hands-on introduction
sidebar:
  exclude: true
start: 2025-02-28T13:00:00
end: 2025-02-28T14:00:00
location: Zoom
zoom: https://ethz.zoom.us/j/64744484699?pwd=YksPvwEp1y9W6Nq9uvegpSrqAfK4ak.1
---

[![ICS file](/calendar-arrow-down.svg)](./invite.ics)

We are pleased to announce the following presentation and hands-on
workshop by
[Laura Kinkead](https://www.datascience.ch/people/laura-kinkead)
and
[Rok Roskar](https://www.datascience.ch/people/rok-roskar)
from the [SDSC](https://www.datascience.ch/).


## Abstract

### Renku 2.0 for Collaborative Research

[Renku](https://blog.renkulab.io/) has evolved considerably over the
past several years; from our strong initial focus on computational
reproducibility to our current goals of fostering collaboration,
transparency, and reusability in all types of data-centric research. In
Renku, researchers can connect data, code, and containerized
computational environments for everything from day-to-day work to
demonstration and publication. On Renku, your project doesn’t exist in
isolation: project assets can be shared and reused with team members or
even made public.

During this presentation, SDSC's Laura Kinkead and Rok Roskar will
introduce the new Renku 2.0 platform, which has just been opened for
early access. Join this hands-on workshop to learn how you could use
Renku in your work and improve collaboration among your team.
