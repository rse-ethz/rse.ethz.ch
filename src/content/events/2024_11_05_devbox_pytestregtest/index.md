---
title: Presentations about "devbox" and "pytest-regtest"
sidebar:
  exclude: true
start: 2024-11-05T11:00:00
end: 2024-11-05T12:00:00
location: Zoom
zoom: https://ethz.zoom.us/j/61451113551
slides: presentations_rse_zoom_call_2024_11_05.zip
---

We are pleased to announce a zoom presentations by Linus Gasser
([C4DT Center For Digital Trust](https://c4dt.epfl.ch/), EPFL),
Roman Wixinger ([Ergon](https://www.ergon.ch/))
and Uwe Schmitt
([Scientific IT Services of ETH](https://sis.id.ethz.ch))
on 5 November, 11-12 (CET time) on Zoom
([https://ethz.zoom.us/j/61451113551](https://ethz.zoom.us/j/61451113551)):

  - Linus and Roman will present an updated version of their
   [RSECon24](https://rsecon24.society-rse.org/) presentation
   “Re-runnable code is all you need”, introducing
   [devbox](https://www.jetify.com/devbox), a tool for creating
   "isolated, reproducible development environments that run anywhere".

  - Uwe will present the
    [pytest-regtest](https://pytest-regtest.readthedocs.io) plugin for
    the Python testing framework [pytest](https://pytest.org),
    which makes it easy to implement tests for complex results, such as
    nested data structures, `NumPy` arrays and `pandas` data frames.


**Please note**: This call will be recorded.

Please use the [ics file](./invite.ics) to add this event to your
calendar.


{{< callout type="info">}}

Although this website (still) has an ethz.ch domain, the community is
not intended to be ETHZ only and we invite everyone who identifies as an
RSE to join us and participate in our events.

{{< /callout >}}

### Abstracts:

#### Re-runnable code is all you need.

Reproducibility is fundamental to scientific integrity but is
increasingly compromised by the common practices of publishing papers
without code, or sharing code that cannot be executed easily on another
computer due to unmanaged dependencies and computing environments.

This
talk introduces the landscape of tools that enable researchers to
overcome these barriers, with a focus on Devbox, a tool designed to
encapsulate computing environments. With Devbox it is easy to ensure
that scientific code is not just runnable but also replicable across
various systems.

By highlighting specific case studies, this presentation
demonstrates how Devbox and similar technologies can transform research
workflows, dramatically saving time with automation, making it easier to
collaborate, and enhancing the credibility of results. Attendees will
gain actionable strategies to implement a culture of reproducibility,
making their research transparent and reproducible with minimal effort.

#### The `pytest-regtest` plugin for the Python `pytest` testing framework

Unlike functional testing, regression testing does not test
whether software produces the correct results, but whether it
behaves as it did before changes were introduced.

[pytest-regtest](https://pytest-regtest.readthedocs.org)
is a plugin for the  popular [pytest](https://pytest.org) testing
framework for Python. The plugin provides snapshot testing, a
method of implementing regression testing by recording results within a
test function and comparing it with previously recorded reference data.

Some  common use cases for snapshot testing are:

  - Providing a safety net when working with code with little or no
    test setup.
  - Testing complex data, such as nested data structures.
  - Testing NumPy arrays or pandas data frames

The presentation will discuss these use cases and show how
`pytest-regtest` can be used in such situations.
