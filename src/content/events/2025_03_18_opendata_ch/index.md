---
title: Opendata.ch and the upcoming Hackathon
sidebar:
  exclude: true
start: 2025-03-18T15:00:00
end: 2025-03-18T16:00:00
location: Zoom
zoom: https://ethz.zoom.us/j/61997988308
---


We are pleased to announce the following presentation by Florin Hasler
([opendata.ch](https://opendata.ch)) and Sebastian Sigloch
([switch.ch](https://switch.ch)) on Zoom on **18 March 15:00-16:00**:

### Abstract

Open data and data reuse enhance research by enabling collaboration,
transparency, efficiency, and innovation.

In 2027 a new law comes into effect mandating the federal government to
publish its data openly by default – a paradigm shift and milestone for
the open data community in Switzerland.

However, research institutions are exempt from this obligation. In this
session, we ask how the Open (Government) Data and the research
community can benefit from each other and how hackathons foster
collaboration and innovation.

**Furthermore, we will present our initial concept of an FAIR/Open
Research Data Hackathon and are looking forward to your feedback and
ideas.**
