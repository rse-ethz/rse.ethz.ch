---
title: "Software development in academia – a personal perspective"
sidebar:
  exclude: true
start: 2024-11-29T11:00:00
end: 2024-11-29T12:00:00
location: VE102, Empa, Dübendorf
zoom: https://teams.microsoft.com/l/meetup-join/19%3ameeting_YjRkNzI5M2UtOTA2OS00Nzc5LWIxODItNjYwODdjZWY5ZDJl%40thread.v2/0?context=%7b%22Tid%22%3a%2257d89df2-dd68-41c1-9b64-81f74f077112%22%2c%22Oid%22%3a%225710dd90-cc68-4ffc-961d-c4e986d182ab%22%7d
---




## Speaker

Dr. Marnik Bercx (Center of Scientific Computing, Theory and Data;
Laboratory for Materials Simulations; PSI).


## Location and Time

Friday, 29 November, 11:00 to 12:00 at VE102, Empa, Dübendorf


## Abstract

Software development in academia is often an underappreciated and
undervalued aspect of scientific research.

Many researchers who contribute to software projects lack formal
training in programming, having picked up coding skills through
necessity rather than education. This results in software that may not
meet high standards of quality, maintainability, or documentation.
Despite its importance, funding agencies often fail to recognize that
software development requires dedicated expertise, resources, and
ongoing maintenance, leading to a reliance on well-meaning individuals
who contribute code in their spare time.

Meanwhile, the academic rewards, such as citations and career
advancement, typically accrue to those producing scientific papers, not
the software that underpins them. Furthermore, many scientists
prioritize research outcomes over software development, seeing the
latter as secondary to their core scientific goals.

However, recent initiatives such as the Research Software Engineer (RSE)
movement are helping address these issues by providing career paths for
software-focused scientists. These efforts are crucial to sustaining the
academic ecosystem, ensuring that software development receives the
attention and support it needs to thrive, and that the infrastructure
supporting modern research remains functional and reliable.
