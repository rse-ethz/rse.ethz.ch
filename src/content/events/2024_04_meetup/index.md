---
title: On-site Meeting April 2024
sidebar:
  exclude: true
start: 2024-04-24T16:00:00
end: 2024-04-24T18:00:00
location: CAB G 61
zoom: https://ethz.zoom.us/j/61286043246

---

The next Meetup will take place on 24th April 16-18 at
[CAB G 61](http://www.mapsearch.ethz.ch/map.do?gebaeudeMap=CAB&farbcode=c010&lang=en)
on the Zurich Zentrum campus.

We will also try to stream the presentations via Zoom at
https://ethz.zoom.us/j/61286043246.

Please fill out the form at https://u.ethz.ch/6931D if you plan to attend in
person. If you are not sure if you have already registered: the service sends
out confirmation emails with the (misspelled) title "Registraion RSE Community
Event April 2024" with uwe.schmitt@id.ethz.ch as the sender.

If you are interested in helping to move the RSE community forward, don't
forget to sign up for some of the working groups. Details and the link to
register can be found at https://rse.ethz.ch/blog/2024_03_26_call_working_groups/.


The planned schedule for the event is

  - 16:00 - 16:15 Updates from the organisers
  - 16:15 - 16:45 Lightning talks, including
      - Julien Dederke from the ETH Library on Open Research Data and the Datastewards network at ETH
      - Andrei Plamada from S3IT (UZH) on "Building interactive content with inseri core for WordPress" ([abstract below]({{< ref "#abstract-inseri-core" >}})).
      - Marco Gähler about his Project https://github.com/gaehlerm/SoftwareEngineering
      - Luisa Barbanti about [R-Ladies Zurich](https://www.meetup.com/rladies-zurich/)
 - 16:45 - 17:15 Adéla Hlobilová (D-BAUG) will present UQLab and UQ[pyLab] ([abstract below]({{< ref "#abstract-uqlab" >}})).
 - 17:15 - 18:00 Breakout discussions / networking

<p>&nbsp;</p>
<hr/>

<h5 id="abstract-inseri-core">
Abstract “Building Interactive Content with inseri core for WordPress”
</h5>

Inseri core is a new tool developed as part of the swissuniversities project
inseri.swiss that aims to allow researchers to create and publish interactive
and executable online content without requiring web development skills. inseri
core is a WordPress plugin under active development that introduces scientific
and interactive components (e.g., Data Table, Plotly Chart, Python Code,
JavaScript Code, Zenodo Repository) to facilitate open science.


<h5 id="abstract-uqlab">
Abstract “Presentation UQLab and UQ[pyLab]”:
</h5>

The Chair of Risk, Safety and Uncertainty Quantification at ETH Zurich develops
two prominent software platforms for uncertainty quantification: UQLab in
Matlab since 2013, and its Python counterpart, UQ[py]Lab since 2021. These
platforms offer robust tools for analysing and managing uncertainties in
various engineering and scientific applications. UQLab provides a comprehensive
suite of algorithms for surrogate modelling, uncertainty propagation,
sensitivity analysis, reliability assessment, Bayesian model calibration and
more; its Python equivalent, UQ[Py]lab, is based on an API called UQCloud, and
Python bindings, opening the platform to a broader user base. To date UQLab has
more than 7,000 cumulated users worldwide from more than 95 countries.
