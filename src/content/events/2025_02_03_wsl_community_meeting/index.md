---
title: RSE Community meeting at WSL
sidebar:
  exclude: true
start: 2025-02-03T13:30:00
end: 2025-02-03T15:30:00
location: WSL Birmensdorf, Englersaal
---


A Research Software Engineer (RSE) is basically anyone who contributes a
significant portion of their time to software/technical development for
research. An RSE can be a scientist with a focus on programming as well
as a software engineer with a connection to science. In the frame of the
ETH Domain ORD Program M2: *“Building a Research Software Engineering
Community to Promote Open Science”*, we are organizing a **first RSE
community meeting at WSL**, in order to introduce and clarify the concept
of RSE at WSL, to connect to the wider RSE community, and to exchange
ideas for the future.



### Agenda:

- 13:30 – 13:35 Welcome to the first RSE@WSL meeting (I. Iosifescu, WSL)
- 13:35 – 14:15 RSE Introductory presentations

  - "The RSE movement - An initiative to emphasize the importance of software in research and academia" (T. Chadha & U. Schmitt, ETH Zurich)
  - “The Development of RSE and Science IT Team at Empa” (A. Bachofner, A. Yakutovich, Empa)

- 14:15 – 15:00 Q&A and plenary discussions (all participants)
- 15:00 – 15:30 RSE networking session (optional)


If the topic of Research Software Engineering resonates with you, we are
looking forward to your participation on **03.02.2025** at
[WSL Birmensdorf](https://www.wsl.ch/de/ueber-die-wsl/standorte/kontakt-und-anfahrt/);
please follow the **blue line** to get to Englersaal from the WSL entrance.

For organizational reasons, please register until 29.01.2025 using this
link:
https://www.wsl.ch/de/ueber-die-wsl/veranstaltungen-und-kurse/registration-rse-community-meeting/
