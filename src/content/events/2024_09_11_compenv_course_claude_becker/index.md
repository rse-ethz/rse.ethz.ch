---
title: Presentation about the "Basics of Computing Environments for Scientists" course from ISG DPHYS
sidebar:
  exclude: true
start: 2024-09-11T10:00:00
end: 2024-09-11T11:00:00
location: Zoom
zoom: https://ethz.zoom.us/j/65640498974
slides: slides_compenv_presentation_rse_community_call_2024_09_11.zip
---

Dear Members of the RSE Community,

We are happy to invite you to the first event after the summer break.

In the next Community Call on **11 September, 10-11 am** on
[zoom](https://ethz.zoom.us/j/65640498974),

[Claude Becker](https://www.phys.ethz.ch/de/das-departement/personen/person-detail.MTc4ODE0.TGlzdC81MTUsMTE3MjU5OTI5OQ==.html)
from the [IT Services Group of the Physics Department](https://www.phys.ethz.ch/services/physics-isg.html) at ETH Zurich will
present their course ["Basics of Computing Environments for Scientists"](https://compenv.phys.ethz.ch/).
He will give an insight into the background and organisational aspects of the
course, followed by a short tour through the contents.

The preliminary schedule of the event is:

  - 10:00 - 10:10  Introduction and updates by the organisers
  - 10:10 - 10:50  Presentation by Claude Becker:
     - 10-15 Minutes: Background of compenv course
     - 25-30 Minutes: Selected content from the compenv course
  - 10:50 - 11:00  Q&A and discussions


Please use the [ics file](./invite.ics) to add this event to your calendar.
