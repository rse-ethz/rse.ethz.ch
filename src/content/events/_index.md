---
title: Events
toc: true
start: 0000-00-00T00:00:00
---

Although this website (still) has an `ethz.ch` domain, the community is
not intended to be ETHZ only and we invite everyone who identifies as an
RSE to join us and participate in our events.

## Upcoming events

{{< upcoming_events >}}

## Past events

{{< past_events >}}
