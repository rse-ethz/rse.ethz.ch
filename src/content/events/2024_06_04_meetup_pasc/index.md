---
title: On-site Meeting June 2024
sidebar:
  exclude: true
start: 2024-06-04T16:00:00
end: 2024-06-04T19:00:00
location: HG D1.1
zoom: https://ethz.zoom.us/j/66358365076

---

The next Meetup will take place on **4th June 16-19** at
**HG D1.1** in the ETH main building.
The meetup is parallel to the [PASC24](https://pasc24.pasc-conference.org/) conference
and RSEs from other countries will attend also.
We will also stream the presentations via Zoom at https://ethz.zoom.us/j/66358365076.


The schedule for the event is

  - 16:00 - 16:30 Get together
  - 16:30 - 16:45 Introduction
  - 16:45 - 18:00 Presentations:
      - Kirsty Pringle (5 minute lightning talk): "Green RSEs:  Working to reduce the environmental impact of research software"
      - Moritz Mähr (5 minute lightning talk): "Fast prototyping: CollectionBuilder's Minimal Computing Approach to Open-Source Collections and Exhibits"
      - Daniel S. Katz (15 minutes + 5 minutes Q&A): "The history of US-RSE"
      - short break
      - Sebastian Keller (20 minutes + 5 minutes Q&A): "Supporting computational researchers with engineering know-how: the role of the PASC core team at CSCS"
      - Roman Wixinger, Linus Gasser (10 minutes + 5 minutes Q&A): "Put some devbox in your repo!"
  - 18:00 - 19:00 Breakout group discussions about "automated testing"
      - 30 minutes of discussions in small groups
      - 30 minutes for presenting the findings
  - 19:00 - : Socialising and networking at [bQm](https://www.bqm-bar.ch).

Please register using [this link](https://u.ethz.ch/jwPj6) if you plan to join.
