---
title: "Empa Scientific IT Seminar Series: Research Software Engineering seen from Industry"
sidebar:
  exclude: true
start: 2025-02-19T15:00:00
end: 2025-02-19T16:00:00
location: LB 0.11.0, Empa
zoom: https://empa-ch.zoom.us/j/83223414711?pwd=IeLHElhZQweEwT1YdZbtaWVAy6lkfv.1
---


We are pleased to announce the following event from the
RSE community at
[Empa](https://www.empa.ch/) on 19 February 15-16.

**Marco Gähler: "Research Software Engineering seen from Industry"**

The room is already full, but the organizers are offering a zoom link
for remote attendance.

## Abstract

At university, we learn how to write functions and classes, and then we
are expected to write large-scale software. This just doesn't work. The
code I wrote at the end of my studies was terrible. I didn't even know
that my code was bad; I just had this feeling that things were
incredibly difficult. Most PhD students that I know face the same issue,
and it's not their fault. The problem is that we were never taught what
software engineering is all about. In most research groups, there is a
serious lack of knowledge regarding software engineering, which costs a
lot of time and money.

I'm currently writing a book dedicated to myself back then, explaining
everything I learned during the last few years in industry. I also help
some PhD students at ETH improve their code. The progress some of them
made in a few months is amazing, as the basic laws behind proper
software engineering are very easy to learn. You just need someone to
teach them to you.

Now, I would love to discuss with the research groups at Empa about
their state of software engineering and how it could be improved.
