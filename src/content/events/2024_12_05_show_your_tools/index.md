---
title: Show your tools!
sidebar:
  exclude: true
start: 2024-12-05T15:30:00
end: 2024-12-05T17:00:00
location: ETH Zentrum campus
---

The next on-site event will be called *Show your tools!* and
will take place on **5 December** from  **15:30** to **17:00**
in **CHN G42** at the ETH Zentrum campus.
A map showing the location of the building can be found
[here](https://ied.ethz.ch/location/chn-building.html).

The concept is that volunteers will present their favourite tools in a
short teaser talk. This will be followed by a discussion in breakout
groups for a more in-depth introduction and help in setting up a tool.

The following volunteers will present:

  - Edoardo Baldi: "The Zed code editor with built-in AI"
  - Jaime Cardozo: "Cloud Native Buildpacks (https://buildpacks.io)"
  - Johanna Haffner: "pre-commit with ruff and pyright"
  - Leonardo Schwarz: "uv and nox"
  - Kalman Szenes: "The Lazygit interface for git"
  - Philipp Wissmann: "Modern C++ project setup: analysis, compilation &
    packaging"

The preliminary schedule of the event is:

  - 15:15 - 15:30: Get together before the event
  - 15:30 - 15:45: Teaser Talks
  - 15:45 - 17:00: Breakout groups
  - 17:00 - open end: Socializing at [bQm bar](https://www.bqm-bar.ch).

Please register
[here to join the event](https://evaluation-app1.let.ethz.ch/TakeSurvey.aspx?SurveyID=823I957K).


{{< callout type="info">}}

Although this website (still) has an ethz.ch domain, the community is
not intended to be ETHZ only and we invite everyone who identifies as an
RSE to join us and participate in our events.

{{< /callout >}}
