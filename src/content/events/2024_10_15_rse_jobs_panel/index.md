---
title: "Applying for RSE jobs: Panel discussion"
sidebar:
  exclude: true
start: 2024-10-15T16:00:00
end: 2024-10-15T17:30:00
location: ETH Hönggerberg
slides: slides-rse-job-panel.pdf
---

Research Software Engineers (RSEs) work in a variety of software
development-related roles in academia, and this job profile can provide you
with a career option between an academic career and leaving academia to work in
industry.

Join us for an exciting panel discussion about RSE jobs. The panel participants
are involved in hiring RSEs and we want to give them an opportunity to
introduce their respective units and share their experiences in hiring
RSEs.

We hope that the attendees get an insight into the variety of job profiles and
opportunities, as well as required skills and best practices for applying for
an RSE job.

**Registration is required for this event**, and the link to register
can be found below.


## When and Where

The event will take place on 15 October 2024, 16-17:30 in HPM H 33
on ETH Hönggerberg campus:


<center>

<figure>
{{< lightbox src="HPM-building.png" group="gallery" size="250x" >}}
<figcaption style="margin-top: -0.8em;">Click thumbnail to enlarge.</figcaption>
</figure>

</center>


## Participants

We are happy to announce the following panel participants:

- Simon Ebner ([PSI, Paul Scherrer Institute](https://www.psi.ch/))
- Linus Gasser ([C4DT, Center For Digital Trust](https://c4dt.epfl.ch/), EPFL)
- David Meyer ([NEXUS Personalized Health Technologies](https://www.nexus.ethz.ch/), ETH Zurich)
- Dr. Andrei Plamada ([Science IT](https://www.s3it.uzh.ch/), UZH)
- Dr. Christina Schnadt Poberaj ([C2SM, Center for Climate Systems](https://c2sm.ethz.ch/))
- Dr. Peter W.J. Staar ([IBM Research Zurich](https://research.ibm.com/labs/zurich))
- Dr. Thomas Wüst ([Scientific IT Services](https://sis.id.ethz.ch), ETH Zurich)


## Agenda

The (preliminary) schedule for the event is:

- 16:00 - 16:10  Introduction presentation
- 16:10 - 16:30  Individual introductions by panel participants
- 16:30 - 17:30  Panel discussion
- 17:30 - open end  Networking at Alumni Lounge


## Registration

Please use [this form](https://evaluation-app1.let.ethz.ch/TakeSurvey.aspx?SurveyID=l83H3o92) to register
for the event.
