+++
title = 'RSECon24 - The RSE Community conference 2024'
date = 2024-09-12T09:18:35+01:00
authors = ['Linus Gasser', 'Uwe Schmitt', 'Roman Wixinger']
+++

The UK conference for the research software engineering community,
[RSECon24](https://rsecon24.society-rse.org/), was held in
Newcastle-upon-Tyne on 3-5 September. The yearly conference is organised
and run by a team of volunteers from the Society for Research Software
Engineering [RSESoc](https://society-rse.org/) and is offered as a
hybrid conference with 400 attendees onsite and around 80 online this
year.

The organisers aim to provide a relevant, inclusive and engaging
conference experience for the RSE community, with a balanced and varied
programme that caters for all roles and skill levels, celebrating
achievements and showcasing impactful software in research.

An important aspect of this is to create an environment where people
from all backgrounds can fully participate and contribute, ensuring that
each attendee feels valued, respected and empowered to learn and grow.

<br/>

<center>
{{< lightbox src="conferce_picture.png" group="gallery" size="400x" >}}
</center>


## Personal impressions Uwe Schmitt

This was my third time at the conference and I always appreciate and
enjoy the welcoming and open atmosphere. My focus is less on the
technical presentations and workshops and more on discussing and sharing
experiences and challenges in my work as an RSE and, since last year,
connecting with and learning from the community building experiences of
many of the attendees.

My visit began with the Monday satellite event for RSE Leaders and
Aspiring Leaders, which offered a series of lightning talks and engaging
breakout discussions on "What makes a good leader?".

The most important sessions for me were the two RSE Worldwide sessions
with participants involved in building RSE communities from Africa,
Asia, the USA, the UK, Belgium, Germany, the Netherlands, Finland and
Switzerland.

I also attended Linus and Roman's presentation "Re-runnable Code is all
you need", which also sparked lively questions and discussion and I hope
that Linus and Roman will give the presentation at one of our community
calls. Due to scheduling conflicts, I was not able to attend all the
sessions I was interested in, but the session I regretted missing the
most was the
[BoF](https://en.wiktionary.org/wiki/birds-of-a-feather_session) session
["Project Management in Research Software - People and
Processes"](https://virtual.oxfordabstracts.com/#/event/49081/submission/111).

As in previous years, I benefited a lot from this one week to get away
from my routine and busy schedule, to take a fresh look at my work and
to get ideas and inspiration. Are you interested in joining next year?
Save the date, RSECon25 is taking place on 8-10 September 2025 at the
University of Warwick.

## Personal impressions Roman Wixinger
To me, RSECon24 was a great opportunity to learn more about creating
reproducible computing environments and share my learnings of the last
few years together with Linus in a talk. Linus presented in person and
I joined the conference remotely. If I had to mention three
things that I liked especially about the conference and our community,
then it would be collaboration, engagement and learning.

In the weeks before the presentation, people from the community supported
our preparations for the talk by sharing their experiences with tools
like devbox. Most prominently, Jaime Cardozo created a
 [demo](https://github.com/eth-library/devbox-spring-demo) of devbox
with Spring Boot that we could use in the presentation. I highly
recommend checking his demo, it is the first demo I tested that worked
on the first try.

After our presentation, I was overwhelmed by the number of questions and
comments by people from the audience. Having people participate in the
discussion and not just consume the information was great to see,
especially because I did not expect to see this level of engagement.

Last, preparing the proposal for the talk and organising everything was
a great learning experience to me. This was my first submission for a
talk at a conference, so many things were new to me. However, this experience
showed me that it is indeed possible and that there is a whole community
at ETH that is here to support you in the process.

Overall, I really enjoyed the experience and would love to see
more of our members at RSECon25 next year!

## Personal impressions Linus Gasser

This was my second RSE-conference, again in Newcastle like in 2022.
I went there together with Ahmed from my team, who is also an RSE.
It was much fun gathering with other RSEs, discussing projects,
hanging out, participating in the social gatherings (though I missed
the Turkish Baths...), and learning new things.
I just love the British architecture of the red brick houses.
And running along the [Ouse Burn](https://jesmonddene.org.uk/)
with Mahmoud was so nice!

The highlight was of course our talk with Roman on
[Re-runnable code is all you need](https://drive.google.com/file/d/1V9oKJljOP8V3j62ruX3SISU5vzHD8FTm/view?usp=sharing), and it went great!
Even with the hybrid setting, Roman presenting from Zurich,
and me in Newcastle, all was smooth and the audience was very
engaged in the topic.
We see now other projects using [devbox](https://www.jetify.com/docs/devbox/quickstart/)
and hope that this will be a small participation for making RSE
projects more reproducible.

Besides that I also tried to spread the love for [Matrix](https://matrix.org/)
and had fun helping the [R contributors](https://contributor.r-project.org/)
setting up their first matrix room and linking it to their slack channel.
Let's hope others will follow :)

Thanks to Uwe for the great beer-places and Indian food...
