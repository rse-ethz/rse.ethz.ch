+++
title = 'Code Review Day on 28th of August'
date = 2024-07-27T09:18:35+01:00
authors = ['Marco Gaehler']
+++

Dear RSE members,

Have you ever had the feeling that you need way too much time to understand
what your code does? Then I am happy to announce the next code review day on
Wednesday 28th of August at the HIT building. We'll be looking at your code and
help you to improve it.

Common topics so far were:
- writing unit and functional tests, a simple way to improve your code
- what data structures to use for which problem
- and many more

Write me an email if you want to participate, [marco.gaehler@gmx.ch](mailto:marco.gaehler@gmx.ch). Most topics are also explained in my book available at https://github.com/gaehlerm/SoftwareEngineering and let me know if you have some feedback on my book.

Enjoy the summer,

Marco
