+++
title = 'Call for funding RSE Ambassadors'
date = 2025-02-21T09:18:35+01:00
authors = ['Uwe Schmitt']
+++

Dear all,

we are pleased to announce that
the [EnhanceR association](https://enhancer.ch) offers funding for
RSE ambassadors from academic/research institutions for one year:

  - Institutions which are members of the EnhanceR association with
    10,000 CHF,
  - Institutions which are not EnhanceR members with 5,000 CHF.

RSE Ambassadors advocate for and support the Research Software Engineer
(RSE) community. They play a crucial role in raising awareness about the
importance of research software engineers in academia and research
institutions.

RSE Ambassadors facilitate networking and collaboration
within the community. They also engage in organizing meetings, outreach
activities,  sharing best practices and help to increase the visibility
of the RSE community in Switzerland.

We particularly encourage applications from volunteers from academic
institutions with no or very early-stage RSE communities.

You can find all details about this call
[this document](./enhancer_call_rse_ambassadors.pdf).

The deadline for submission is **23 March 2025**.
