+++
title = 'The website is ready for community contributions!'
date = 2024-03-17T09:18:35+01:00
authors = ['Uwe Schmitt']
+++

We migrated the previous version of the website (which was a bunch of HTML
files + some CSS) to [HUGO](https://gohugo.io/) for managing and rendering the
content from Markdown to HTML. The new setup allows integration of community
contributions, such as blog posts using merge requests.

<!--more-->

### The new setup

For styling we are using a slightly modified version of the
[Hextra theme](https://imfing.github.io/hextra). The modifications are hosted
at https://gitlab.com/rse-ethz/hugo-hextra-modified.
You can find the setup including the content at https://gitlab.com/rse-ethz/rse.ethz.ch.
The [README.md](https://gitlab.com/rse-ethz/rse.ethz.ch/-/blob/main/README.md?ref_type=heads)
in this repository also describes local setup and further details.

The CI setup builds and publishes the web site automatically:

<center>

{{< lightbox src="ci_in_action.png" size="600x">}}

</center>

### Contribute!

Do you want to improve the current `README.md`, write a blog entry, or add to the
pages on the site? Create a merge request and make https://rse.ethz.ch a little
better!
