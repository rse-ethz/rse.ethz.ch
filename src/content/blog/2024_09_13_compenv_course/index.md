+++
title = '"Basics of Computing Environments for Scientists" course from ISG DPHYS'
date = 2024-09-13T09:18:35+01:00
authors = ['Uwe Schmitt']
+++

Do any of the following statements sound familiar to you or someone you know?

-   I have taken a Python course but am having problems with the setup or the correct way to install packages.
-   I want to know how to use my computer's command line efficiently.
-   I'm interested in Python tools, e.g. for checking Python code.
-   I'm not sure which editor or development environment to use for Python development.
-   I'm interested in best practices to increase my productivity when working with Python.


The [lecture "Basics of Computing Environments for Scientists"](https://www.vorlesungen.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?lerneinheitId=183564&semkez=2024W&lang=en)
from the IT Support group of the ETH Physics department covers these and many other topics that are often missing in many programming courses.

The lecture is open for all members of ETH Zurich and the lecture notes are available at [https://compenv.phys.ethz.ch/](https://compenv.phys.ethz.ch/).
