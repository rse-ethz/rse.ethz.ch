+++
title = 'RSE book club'
date = 2024-06-04T09:18:35+01:00
authors = ['Marco Gaehler']
+++

Hi everyone,

I'm Marco and would like to organise a book club for the RSE community.
The idea is to meet every two weeks to discuss a chapter from my book about Software Engineering.

<!--more-->


The book is in development and available [at github](https://github.com/gaehlerm/SoftwareEngineering).

The preliminary schedule is to meet for one hour every second Thursday from
12:30 to 13:30 on zoom. The first meeting would be on June 20. Even if you
don't have time then, you can still sign up. Maybe we'll change the schedule at
some point.

In the first meeting we'll have a look at the chapter about
[classes](https://github.com/gaehlerm/SoftwareEngineering?tab=readme-ov-file#14-classes)
and best practices on how to use them. Feel free to read other books as well or
bring along some example code you would like to discuss. Though the code
snippets should be fairly short (< 50 lines) or we won't be able to cover them
in one hour.

In case you are interested, please use [this form](https://evaluation-app1.let.ethz.ch/TakeSurvey.aspx?SurveyID=p323562K) to
register. In case you would like to join but the proposed time does not suit
you, please let us know in the comments of the registration form.

Cheers,
Marco
