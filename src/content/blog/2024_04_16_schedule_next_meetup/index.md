+++
title = 'Schedule Meetup on 24th of April 2024'
date = 2024-04-16T10:44:33+01:00
authors = ['Uwe Schmitt']
+++

We published the detailed schedule for the next meetup in our [events
section](/events/2024_04_meetup).
