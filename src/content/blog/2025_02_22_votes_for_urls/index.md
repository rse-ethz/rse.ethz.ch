+++
title = 'Community voting results for the domain name of a Swiss-wide RSE website'
date = 2025-02-20T09:18:35+01:00
authors = ['Uwe Schmitt']
+++


The results are in, and the winner (🥁🥁🥁🥁) is
<code style="font-size:120%">rse.swiss</code>!

Together with [EnhancerR](https://enhancer.ch)
we will now apply for the domain name `rse.swiss`.
The process is a bit complicated (see
[here](https://www.nic.swiss/nic/de/home/registrieren-sie-ihre-swiss-domain/wen-kontaktieren-.html)) and
and if our application does not go through, we would proceed with the
second place winner `rse-ch.org`.

We got 79 responses in total, the average rankings for the offered
options are (lower is better):

| Domain name     | Average Ranking      |
|-----|------|
|rse.swiss       |   2.2|
|rse-ch.org      |   2.43|
|swiss-rse.org   |   2.52|
|ch-rse.org |   2.85|

Some people have asked why we have not offered
`rse.ch` as an (obvious) option.
The simple answer is, that this domain name is not available and the
current owner did not show any interest in selling the name.
