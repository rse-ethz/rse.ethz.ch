+++
title = 'New Matrix space for RSE activities in Switzerland'
date = 2024-03-20T19:28:35+01:00
authors = ['Uwe Schmitt']
+++

Together with our colleagues from  [EPFL](https://rse.epfl.ch) we decided
to create a new Matrix space
`#rse-ch:matrix.org`
to host all current and future RSE activities within Switzerland.  The
rooms from the previous space still exist, but are now also registered
within the new space.

<!--more-->

You can use the ETH service
[https://m.ethz.ch/#/#rse-ch:matrix.org](https://m.ethz.ch/#/#rse-ch:matrix.org)
to join if you have not already set up a client. All others can add a new space in their client using the
identifier `#rse-ch:matrix.org`.

<br/>

<center>

{{< lightbox src="rse_space.png" size="x240" >}}
{{< lightbox src="rooms.png"  size="x240">}}

</center>

In case you joined the previous `ETH-RSE` space:

  - Please join the new space.
  - You don't need to leave any room.
  - Remove the previous space from your client.

We have no deadline for closing the old space, but encourage all existing
members to subscribe to the new space `#rse-ch:matrix.org`.
