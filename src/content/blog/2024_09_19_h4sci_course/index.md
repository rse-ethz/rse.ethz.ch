+++
title = '"Hacking4Sciences" course from MTEC'
date = 2024-09-19T09:18:35+01:00
authors = ['Minna Heim']
+++

### Do any of the following statements sound familiar to you or someone you know?
-   I have a non-computer science background.
-   I want to incorporate programming into my approach to working with data.
-   I want to be part of the open source ecosystem.

If so, the [lecture "Hacking for Sciences – An Applied Guide to Programming with Data"](https://www.vvz.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?lerneinheitId=181821&semkez=2024W&ansicht=LEHRVERANSTALTUNGEN&lang=en) by Dr. Matthias Bannert from the Swiss Economic Institute (KOF) at MTEC-ETH might be the right fit for you.

### Learn how to construct a Cloropeth map:
By taking this course, you can learn how to better manage and visualise data. An example of what you could learn is how to construct a Cloropeth map: which maps the values of a variable available by region to a given continuous colour palette on a map.

![image](swiss_cloropleth_map.png)

To construct this map, you can get a definition of the shape of a country, here we use GeoJSON. Then you assign values to the different regions, and finally you can visualise this using the charting function from the {echarts4r} package. Below is the code used to create this map.

```r {class="my-class" id="my-codeblock" lineNos=inline tabWidth=2}
library(echarts4r)
library(viridisLite)
library(jsonlite)

json_ch <- jsonlite::read_json(
  "https://raw.github..."
)


d <- data.frame(
  name = c("Zürich",
           "Ticino",
           "Zentralschweiz",
           "Nordwestschweiz",
           "Espace Mittelland",
           "Région lémanique",
           "Ostschweiz"),
  values = c(50,10,100,50,23,100,120)
)

d |>
  e_charts(name) |>
  e_map_register("CH", json_ch) |>
  e_map(serie = values, map = "CH") |>
  e_visual_map(values,
               inRange = list(color = viridis(3)))
```

The full interactive example can be found in the [Hacking4Sciences course book](https://rse-book.github.io/case-studies.html#sec-map)


This highly interactive, block-style online course provides valuable big-picture guidance, teaches practical software development skills, and allows you to test your knowledge through a final group coding project. The course is open to PhD students at ETH, and interested guests or students at other levels are also welcome.


To learn more about the course and its philosophy, please visit the h4sci website https://h4sci.github.io.
