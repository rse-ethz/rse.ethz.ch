+++
title = 'Schedule Meetup on 4th of June 2024'
date = 2024-05-19T10:44:33+01:00
authors = ['Uwe Schmitt']
+++

We published the detailed schedule for the next meetup in our [events
section](/events/2024_06_04_meetup_pasc).
