+++
title = 'Applying for RSE jobs: Panel discussion'
date = 2024-10-26T09:18:35+01:00
authors = ['Fabian Balk','Leonardo Capitani','Ionuț Iosifescu','Gérôme Meyer','Roman Wixinger']
+++

[![](https://zenodo.org/badge/DOI/10.5281/zenodo.14018626.svg)](https://doi.org/10.5281/zenodo.14018626)



{{< callout >}}

This blog entry is a collaborative effort by members of the
audience to consolidate their personal notes and impressions
from the event on 15 October 2024 at ETH Zurich.

For more details about the event, including a full list of panellists,
please visit the more detailed
[event page ](/events/2024_10_15_rse_jobs_panel).

The impressions below are therefore based on the discussions within the
panel and with the audience and may be different for other employers.
Nevertheless, the panel agreed on most of the points below.

{{< /callout >}}


Research Software Engineers (RSEs) work in a variety of software
development-related roles in academia, and this job profile can provide
you with a career option between an academic career and leaving academia
to work in industry. However, since this role is only recently gaining
recognition, there is still a lot of confusion about it in the
scientific community.

Essentially, the question of whether you are an RSE already, without
realising, can be answered fairly simply: If you apply coding expertise
or develop research software for advancing research and you enjoy doing
so, then you are an RSE.

But what exactly does a job as an RSE entail, and how to secure an RSE
position? To shed some light on these principal questions the RSE@ETH
hosted a panel discussion titled “Applying for RSE Jobs“ on October 15,
2024, at the ETH Hönggerberg campus.

The event gathered a diverse group of professionals actively involved in
hiring RSEs from different organisations, including Paul Scherrer
Institute (PSI), EPFL's Center for Digital Trust (C4DT), the Center for
Climate Systems Modelling (C2SM, ETHZ), the Scientific IT Services of
ETH, the Science IT unit of the University of Zurich (UZH), Nexus and IBM
Research Zurich.

During the discussion, the panellists addressed key
questions, such as:

  - How can you make your application stand out when applying for RSE
    positions?

  - What skills and experiences are most valued by employers hiring RSEs?

  - How can you find a position that aligns with your career goals and
    offers a healthy work-life balance?

In this blog post, we aim to summarise the most important insights from
the panel, sharing everything from the no-gos to actionable advice on
how to get started in your job search.

## Key lessons

The following insights are based on the discussions and answers
among the panellists.

### The two extremes: Scientific programmer vs. Software engineer

RSEs tend to work between two extremes. On one end, as scientific
programmers they can come from a specialised background where,
they conduct research directly and use
code as their primary tool to do so. One the other end, as software
engineers they tend to take on a service role,
supporting other researchers with their work. They often come from
various backgrounds and their day-to-day business includes preparing
research code (e.g. performance optimisations), applying DevOps
practices to deploy the code, as well as monitoring and maintaining
the resulting product.

Most RSEs don’t fall neatly into one or the other category, but will
tend towards one. Before applying for a position, try to figure out
which of the two you prefer and whether the RSE position you are looking
at matches your expectation.

Bonus tip if you are unsure: Talk to RSEs who are already a few years
“ahead” of you in their career and have a similar background to you.
Many will be happy to guide you!

### Demonstrating your skills beyond your CV

Just like for scientists and software engineers it pays to build up a
portfolio that you can show in public. A CV in the classic academic
style, e.g. listing publications and attended scientific conferences,
tends to be less meaningful for an application to an RSE position. We
RSEs are in the lucky position that we get to use both scientific and
code contributions to our advantage. For example, include papers or
public GitHub repositories that you have worked on in your CV or
motivation letter. Also contributions to open source projects, relevant
blog articles, talks or engagement in a local RSE group can make your
application stand out.

An opportunity is just around the corner: this December, our community
is organizing an event on software tools. Sharing your favorite tool or
participating in the discussions can be a great way to get started.

### What hiring managers are looking for

First off, communication and presentation skills can be as important as
technical skills. You can build them with community work, by reaching
out to people for informal discussions over coffee, taking courses
(for example at the Language Center) and other ways you feel comfortable
with. Since an RSE has to work directly with and for scientists,
understanding their requirements and communicating the coding progress
well is particularly important.

On that note, hiring managers want to see genuine interest and people
who can get excited about projects that are not their own and might even
be outside their comfort zone in terms of expertise. The service
mentality to provide coding expertise (companies, researchers etc.) is
in the foreground.

In many projects you will mostly work by yourself
for the client, so independent work and self-management are often
equally important. Hiring managers search for “pragmatism” to solve
day-by-day problems. Having worked in industry after academia can be a
plus in your CV.

Also note that an RSE application should not resemble a Postdoc
application. Focus less on scientific achievements and instead highlight
and prove your technical and interpersonal skills.


### Application strategies and job interviews

In the haste of today’s job environment it might be tempting to send out
as many applications as possible, hoping that one will stick. But this
can actually work against you as multiple applications to the same
company or even group are often  noticed and can make you seem
desperate.

Focusing on nailing a single application will increase your chances of
actually getting an interview.

The panellists had different opinions about the importance of the
motivation letter. Some argued that in the age of Chat GPT, the
motivational letter has lost its importance, while others felt that a
personalised motivational letter can still stand out.

In order to nail your application, you can try to figure out who will
read your application and address them directly. Try to personalise your
application towards the job in obvious ways to show that you really care
and you are not just sending generalised applications to many different
positions.

And note that different interviewers care about different things: some
prefer evidence of prior work in the form of published papers or
open-source contributions while others put more emphasis on the
motivation letter or a presentation during the interview. If the
interviewer’s contact is listed in the job opening, try to contact them
and ask what they care about most. This can also double up as making a
good first impression.

During the interviews, some employers do coding tasks, life or offline
via email, pair programming, or have candidates debug code. Some also
ask candidates to present a topic that they must prepare.

When you get a chance to present yourself during an interview, speak out
loud to show what you are thinking during an interview, especially if
you are unsure about the answer. The interviewers are more interested in
your thought process than the actual solution. Also use coffee breaks on
the “interview day”  to show genuine interest and get to know the group.
This also helps you decide if they are a good fit.

If ever an interview does not work out, asking your interviewer whether
they know of other positions in the company that you might be a good fit
for can even give you an edge for that position as you might get an
internal recommendation.

### Work-life balance

A work-life balance, which works out for you personally, is highly
dependent on your needs. On one side, most academic institutions allow
for part-time positions and do not ask you to work outside working
hours.

On the other side, some companies understand the RSE role as a
technical consultant, where duly project completion and service
mentality are prioritised.

This being said, some companies allow for
flexible planning schedules to fit your personal circumstances.
Ultimately, your job should fit your private life, so it allows you
to follow your personal  interests, not vice versa.

Work-life balance is important: Most institutions in the academic
environment allow for part-time positions and want you to rest outside
of working hours. Some companies give you a lot of freedom in planning
your schedule.

### On the job (and permanent positions)

In many RSE or Scientific IT groups, there is a mix of temporary and
permanent positions.

The question of securing a permanent versus a temporary position is
highly dependent on your level of experience. If you are just starting
your career, you may not need to worry about the
contract duration of a job offer.  At this stage of your career you can
dare to try something new and completely different, as long as it makes
you happy. Search for a job, where you can identify with the company’s
values and the position appears exciting and interesting to you. It will
pay off to show that you are doing your best and are motivated to learn
and improve.

Be active and dare to speak your mind, if you think there
is a better solution to a task. Someone will notice your efforts - and
who knows? Maybe you will get offered a permanent position as a result
of your work.

In response to the question, “What skills do you think are most lacking
in junior RSEs?“, pragmatism came up. Looking for simple solutions that
meet the given requirements and are maintainable is more appreciated
than building an all-bells and whistles solution that goes over budget
and turns out to be a maintenance nightmare.

In some situations, you may need to say “no” if a client asks for
something that is not
reasonable and you can propose a  better alternative. Instead of “No“
you can also say “Yes, but …“ if you are uncomfortable saying “No“.

## Conclusion

The role of the Research Software Engineer is still emerging and often
goes by various titles such as Scientific Programmer, Software Engineer
or Data Scientist. This diversity in naming can make it challenging to
find and identify RSE job postings. We hope that, moving forward, the
umbrella term “RSE“ will gain broader recognition, simplifying the job
search process for aspiring professionals in this field.

Within our community, we believe that Research Software Engineering is a
crucial component of successful research in today's digitalised world.
Nearly every scientific discipline now relies heavily on sophisticated
methods for data collection and analysis, from high-performance
computing (HPC) clusters to automated experimental setups.

As the demand for advanced software solutions in research grows, RSEs
play an important role in ensuring that scientific investigations are
not only reproducible but also have a meaningful impact. This increased
reliance on software underscores the RSE career path as both promising
and essential for the future of research. This also means that RSE
positions are available in fields you never would have thought to work
in but perfectly fits your set of programming skills. Therefore, think
closely about what your software skills are, what skills you want to
acquire and how the job at hand aligns with that.

We hope that the insights and lessons shared from our panel discussion
will be valuable to you as you consider your future career options.
Whether you are contemplating becoming an RSE or exploring related
positions like Data Scientist in industry, these perspectives can guide
you in making informed decisions and help you navigate the exciting
opportunities that lie ahead.
