{{ .Section }}
{{ if eq .Section "events" }}
    BEGIN:VCALENDAR

    {{ .Section }}

    {{/* Configure the calendar templates with our actual values */}}
    {{/* Multiline hack from: https://github.com/gohugoio/hugoDocs/issues/764#issuecomment-625711237 */}}
    {{ with dict `` `
    ` "name"         (dict "text" "Example Events" "lang" "en-GB") `` `
    ` "description"  (dict "text" "Example Events Calendar" "lang" "en-GB") `` `
    ` "url"          (.OutputFormats.Get "HTML").Permalink `` `
    ` "color"        "crimson" `` `
    ` "uid"          .File.UniqueID `` `
    ` "lastmod"      .Lastmod `` `
    ` "source"       (.OutputFormats.Get "Calendar").Permalink `` `
    ` -}}
    {{ partial "ical/cal_props.ics" . }}
    {{ end }}
    BEGIN:VTIMEZONE

    TZID:Europe/Zurich
    TZURL:http://tzurl.org/zoneinfo-outlook/Europe/Zurich
    X-LIC-LOCATION:Europe/Zurich

    BEGIN:DAYLIGHT
    TZNAME:CEST
    TZOFFSETFROM:+0100
    TZOFFSETTO:+0200
    DTSTART:19700329T020000
    RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU
    END:DAYLIGHT

    BEGIN:STANDARD
    TZNAME:CET
    TZOFFSETFROM:+0200
    TZOFFSETTO:+0100
    DTSTART:19701025T030000
    RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU
    END:STANDARD

    END:VTIMEZONE


    {{/* Configure the calendar templates with our actual values */}}
    {{/* Multiline hack from: https://github.com/gohugoio/hugoDocs/issues/764#issuecomment-625711237 */}}
    {{ with dict `` `
    ` "description"  (dict "text" .Plain "lang" "en-GB") `` `
    ` "summary"      (dict "text" (printf "RSE: %s" .Title) "lang" "en-GB") `` `
    ` "eventStart"   (dict "dateTime" .Page.Params.start "timeZoneID" "Europe/Zurich") `` `
    ` "eventEnd"   (dict "dateTime" .Page.Params.end "timeZoneID" "Europe/Zurich") `` `
    ` "location"     (dict "text" .Params.location "lang" "en-GB") `` `
    ` "onlinemeetingexternallink"          (.Params.zoom | default "-") `` `
    ` "contact"      (dict "text" (printf "%s: %s" .Params.orga .Params.orgaEmail) "lang" "en-GB") `` `
    ` "color"        "crimson" `` `
    ` "status"       (cond (.Params.cancelled | default false) "CANCELLED" "CONFIRMED") `` `
    ` "uid"          .File.UniqueID `` `
    ` "timestamp"    .Date `` `
    ` "created"      .Date `` `
    ` "lastmod"      .Lastmod `` `
    ` -}}
    {{ partial "ical/comp_event.ics" . }}
    {{ end }}


    END:VCALENDAR
{{ end }}
