import pathlib
import re
from datetime import datetime

import click

TEMPLATE = """
---
title: "{title}"
sidebar:
  exclude: true
start: "{start}"
end: "{end}"
location: "{location}"
{zoom}
---

We are pleased to announce the event *"{title}"*
at {location}
on **{start_date_time}*:

### Abstract

Abstract about "{title}" here.

CHECKLIST:

  - put 'invite.ics' file into this folder!
  - did you include speaker information, date ane time?
  - did you include registration information (if needed).
  - double check everything!

""".lstrip()


@click.command()
@click.option(
    "--date",
    type=click.DateTime(formats=["%Y-%m-%d"]),
    required=True,
)
@click.option(
    "--time-start",
    type=click.DateTime(formats=["%H:%M"]),
    required=True,
)
@click.option(
    "--time-end",
    type=click.DateTime(formats=["%H:%M"]),
    required=True,
)
@click.option(
    "--title",
    type=str,
    required=True,
)
@click.option(
    "--location",
    type=str,
    required=True,
)
@click.option("--zoom-link", type=str)
def main(date, time_start, time_end, title, location, zoom_link):
    time_start = time_start.time()
    time_end = time_end.time()

    slug = re.sub(r"[^ -~]", "_", title.lower().replace(" ", "_"))
    here = pathlib.Path(__file__).parent.absolute()
    folder = (
        here
        / "src"
        / "content"
        / "events"
        / f"{date.year}_{date.month:02d}_{date.day:02d}_{slug}"
    )
    if folder.exists():
        raise ValueError(f"folder {folder} already exists")

    folder.mkdir()
    print(f"created folder {folder}")

    zoom = f"zoom: {zoom_link}" if zoom_link else ""
    start = datetime.combine(date, time_start)
    end = datetime.combine(date, time_end)

    start_date_time = start.strftime("%A, %-d %B, from %-H:%M ") + end.strftime(
        "to %-H:%M"
    )

    (folder / "index.md").write_text(
        TEMPLATE.format(
            zoom=zoom,
            start=start.isoformat(),
            end=end.isoformat(),
            title=title,
            location=location,
            start_date_time=start_date_time,
        )
    )

    print(f"wrote {folder}/index.md")


if __name__ == "__main__":
    main()
