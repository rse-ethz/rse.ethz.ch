import pathlib
import smtplib
from email.message import EmailMessage

import click
import frontmatter
import markdown

SMTP_SERVER = "smtp0.ethz.ch"
FROM = "schmittu@ethz.ch"
TO = "rse-community@sympa.ethz.ch"


@click.command()
@click.argument("folder")
def main(folder):
    """
    send event announcement email to mailing list
    """
    folder = pathlib.Path(folder)
    if not folder.is_dir():
        click.secho(f"error: {folder} is not a folder")
        return 1

    index_files = list(folder.glob("*index.md"))
    if not index_files:
        click.secho(f"error: {folder} does not contain an index.md or _index.md file")
        return 1
    if len(index_files) > 1:
        click.secho(f"error: {folder} contains multiple index.md or _index.md files")
        return 1

    fm = frontmatter.load(index_files[0])
    title = fm.get("title", "").strip()
    start = fm.get("start")
    end = fm.get("end")
    location = fm.get("location", "").strip()
    zoom = fm.get("zoom", "").strip()

    ok = True
    if not title:
        click.secho("frontmatter: title is missing or empty")
        ok = False
    if not start:
        click.secho("frontmatter: start is missing or empty")
        ok = False
    if not end:
        click.secho("frontmatter: end is missing or empty")
        ok = False

    if not location and not zoom:
        click.secho("frontmatter: location and zoom are both is missing or empty")
        ok = False

    if start.date() != end.date():
        click.secho("error: cannot handle events which end on different day than start")

    ics_files = list(folder.glob("*.ics"))
    if not ics_files:
        click.secho("error: folder does contain ics file")
        ok = False
    if len(ics_files) > 1:
        click.secho(f"error: {folder} contains multiple ics files")
        return 1

    if not ok:
        return 1

    content = ""
    LB = "  \n"  # markdown line break

    content += f"**What**: *{title}*{LB}"
    content += f"**When**: {start:%A %Y-%m-%d, %H:%M}-{end:%H:%M} CET{LB}"
    if location:
        content += f"**Location**: {location}{LB}"
    if zoom:
        content += f"**Zoom**: [{zoom}]({zoom}){LB}"

    content += "---\n"
    content += fm.content

    return send_email(title, content, ics_files[0])


def send_email(title, content, ics_file):
    html = markdown.markdown(content)

    smtp = smtplib.SMTP(SMTP_SERVER, timeout=3.0)

    msg = EmailMessage()
    msg.set_content(content)
    msg.add_alternative(html, subtype="html")
    msg["subject"] = title.strip()
    msg["From"] = FROM
    msg["To"] = TO
    msg.add_attachment(ics_file.read_text(), "calendar", filename="rse-event.ics")

    smtp.send_message(msg)

    click.secho("email sent.")
    return 0


if __name__ == "__main__":
    main()
