#!/usr/bin/env bash

set -e
( set -e; cd src; hugo; )
set +e

LOG=$(mktemp)


EXITCODE=0
RED="\e[33m"
ENDCOLOR="\e[0m"

for p in $(find src/public/ -name "*.html" | sort); do
    cat $p | ( cd spellchecker; hunspell  -H -l -d en_GB-ise | grep -v -f ./spellchecker-ignore.txt ) > $LOG
    if test -s $LOG; then
        plocal=$(echo $p | sed -e "s/public/content/g")
        filename=$(basename $plocal)
        folder=$(dirname $plocal)
        if test $filename = index.html; then
            if test -f $folder/index.md; then
                md_file=index.md
            else
                md_file=_index.md
            fi
        else
            md_file=$(echo $filemane | sed -e "s/html$/md")
        fi

        echo -e "errors in $folder/$md_file:${RED}"
        cat $LOG | sed -e "s/^/    /g"
        echo -n -e ${ENDCOLOR}
        EXITCODE=1
    fi
done;

exit $EXITCODE

# spellchcker-cli docs at
# https://www.npmjs.com/package/spellchecker-cli

npx spellchecker \
  --dictionaries spellchecker-ignore.txt \
  --language en-GB \
  --plugins spell \
    indefinite-article \
    frontmatter \
    syntax-urls \
  --frontmatter-keys title \
  --files $* > ${LOG}

# npx does not forward exit code for whatever reason, so we manage this
# manually:

cat $LOG
grep --silent warning ${LOG} && exit 1

exit 0
